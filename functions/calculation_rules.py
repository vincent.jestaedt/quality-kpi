"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

#Berechnungsfunktion des Gesamtpreises
def kpi_price(system: LegoAssembly)->float:
    """
    Calculates the total price of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_price (float): Sum of prices of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
#eine Überprüfung der Daten auf Verwendbarkeit
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_price()")
#eine Schleife summiert die einzelnen Preise der Bauteile auf 
    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
    return total_price 

#Berechnungsfunktion der maximalen Lieferzeit
def kpi_delivery_time(system: LegoAssembly) -> float:
    """
    Calculates the maximum delivery time of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        max_delivery_time (float): maximum delivery time of all components in the system in days

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
#eine Überprüfung der Daten auf Verwendbarkeit
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")
# Anpassen eines Variablennamens mit dem entsprechenden Eintrag in den Datenblättern   
    delivery_time = 'delivery time [days]'  

#Benutzen der max-Funktion mit einer Lambda-Funktion um das Maximum zu finden
    max_delivery_time = max(system.get_component_list(), key=lambda x: x.properties.get(delivery_time, 0))

# Ausgabe der entsprechenden längsten Lieferzeit
    return max_delivery_time.properties.get(delivery_time, 0)



if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
